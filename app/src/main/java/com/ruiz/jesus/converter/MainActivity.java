package com.ruiz.jesus.converter;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.ruiz.jesus.converter.databinding.ActivityMainBinding;

import org.w3c.dom.Text;

public class MainActivity extends Activity {

    public double miles,kms;
    public String result, miles_string;

    TextView result_tv = (TextView)findViewById(R.id.tv_km);
    EditText miles_ed=(EditText)findViewById(R.id.ed_miles);
    Button button=(Button)findViewById(R.id.btnconvert);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                convertMiles();
            }
        });
    }

    public void convertMiles(){
        //1 mi = 1.609km
        miles_string=miles_ed.getEditableText().toString();
        miles=Double.parseDouble(miles_string);

        kms=miles*1.609;
        result=kms+" KM";
        result_tv.setText(result);
    }
}